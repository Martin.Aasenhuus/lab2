package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated

        Main.pokemon1 = new Pokemon("Pikachu");
        Main.pokemon2 = new Pokemon("Oddish");

        while (pokemon1.isAlive() && pokemon2.isAlive()){
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
            if (!pokemon2.isAlive()){
                System.out.print(pokemon1 + " is defeated by " + pokemon2);
                break;
            }
            else if (!pokemon2.isAlive()){
                System.out.print(pokemon2 + " is defeated by " + pokemon1);
                break;
            }
        }
    }
}



    

