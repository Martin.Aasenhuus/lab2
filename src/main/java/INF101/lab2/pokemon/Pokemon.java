package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String name){
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
        this.name = name;
        }
    
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        if (this.healthPoints == 0){
            return (false);
        } else {
            return (true);
        }
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);

        if (target.isAlive()) {
            System.out.println(this.getName() + " attacks " + target.getName());
        } else {
            System.out.println(target.getName() +" is defeated by " + this.getName() + ".");
        }
    }

    @Override
    public void damage(int damageTaken) {
        // trekke fra damage taken
        if (damageTaken < 0){
            damageTaken = 0;
        }

        int newHp = this.healthPoints - damageTaken;

        if (newHp < 0) {
            this.healthPoints = 0;
        } else {
            this.healthPoints = newHp;
        }

        // ikke gå under tallet 0
        String d = "";

        d += this.name + " takes ";
        d += damageTaken + " damage and is left with ";
        d += newHp + "/" + this.maxHealthPoints + " HP";

        // skriv ut
        System.out.println(d);
    }

    @Override
    public String toString() {
        String s = "";

        s += this.name + " HP: (";;
        s += this.healthPoints + "/";
        s += this.maxHealthPoints + ") ";
        s += "STR: " + this.strength;
        return s;
        
    }

}


